<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [@rt\_lite/common](./common.md) &gt; [TVSeries](./common.tvseries.md)

## TVSeries type

Describes a TV series

**Signature:**

```typescript
export type TVSeries = Media<MediaType.TV_SERIES>;
```
**References:** [Media](./common.media.md)<!-- -->, [MediaType.TV\_SERIES](./common.mediatype.md)

