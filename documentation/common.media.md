<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [@rt\_lite/common](./common.md) &gt; [Media](./common.media.md)

## Media type

Describes a piece of media

**Signature:**

```typescript
export type Media<T extends MediaType> = {
    title: string;
    url: URL;
    score?: number;
    year?: number;
};
```
**References:** [MediaType](./common.mediatype.md)

