import browserslist from 'browserslist';
import consola from 'consola';
import esbuild from 'esbuild';
import { esbuildPluginBrowserslist } from 'esbuild-plugin-browserslist';
import fs from 'fs/promises';
import path from 'path';

// Constants
const CJS_EXTENSION = '.cjs.js';
const CJS_FORMAT = 'cjs';
const ENTRY_POINT = './src/index.ts';
const ESM_EXTENSION = '.esm.js';
const ESM_FORMAT = 'esm';
const OUT_DIR = 'dist';
const PACKAGE = 'package.json';

const getPkg = async () => {
	const pkgString = await fs.readFile(`./${PACKAGE}`, 'utf-8');

	return JSON.parse(pkgString);
};

// A function that copies this project's package.json into the output folder so
// that it may then be published to a package repository
const writePackageJson = async ({
	author,
	bugs,
	dependencies,
	description,
	homepage,
	license,
	name,
	peerDependencies,
	repository,
	type,
	version
}) => {
	const exports = {
		'./': {
			import: {
				types: './index.d.ts',
				default: `./index${ESM_EXTENSION}`
			},
			require: {
				types: './index.d.ts',
				default: `./index${CJS_EXTENSION}`
			}
		}
	};

	// Construct the new package.json as an object
	const newPkg = {
		author,
		bugs,
		dependencies,
		description,
		exports,
		homepage,
		license,
		main: `index${CJS_EXTENSION}`,
		name,
		peerDependencies,
		repository,
		type,
		types: 'index.d.ts',
		version
	};

	// Create the JSON string, indenting by the specified amount
	const spacesToIndent = 2;
	const newPkgString = JSON.stringify(newPkg, null, spacesToIndent);

	// Write the package.json copy to disk
	await fs.writeFile(path.resolve(`./${OUT_DIR}/${PACKAGE}`), newPkgString);
};

// The main build function
const build = async () => {
	consola.info('Beginning build...');

	try {
		const pkg = await getPkg();

		const sharedConfig = {
			bundle: true,
			entryPoints: [ENTRY_POINT],
			external: Object.keys(pkg.peerDependencies ?? {}),
			minify: true,
			outdir: OUT_DIR,
			plugins: [
				esbuildPluginBrowserslist(browserslist(), {
					printUnknownTargets: false
				})
			],
			sourcemap: true
		};

		const [
			writeCJSBuild,
			writeESMBuild
		] = [
			[CJS_EXTENSION, CJS_FORMAT],
			[ESM_EXTENSION, ESM_FORMAT]
		].map(([extension, format]) => async () => {
			const upperFormat = format.toUpperCase();

			consola.info(`${upperFormat} build starting`);

			await esbuild.build({
				...sharedConfig,
				format,
				outExtension: {
					'.js': extension
				}
			});

			consola.success(`${upperFormat} build done`);
		});

		await Promise.all([
			writeCJSBuild(),
			writeESMBuild()
		]);

		// Copy package.json
		consola.info('Writing package.json');
		await writePackageJson(pkg);
		consola.success('Wrote package.json');

		consola.success('Complete!');
	} catch (err) {
		// Log any errors
		// eslint-disable-next-line no-console
		console.error(err);

		// And exit with an error status
		process.exit(1);
	}
};

// Run build
build();
