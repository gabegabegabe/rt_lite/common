import type { MediaType } from '~/models/media-type';

/**
 * Describes a piece of media
 *
 * @public
 */
// Type variable is currently unused but here on the assumption that there could
// be meaningful differences in the future.
// eslint-disable-next-line @typescript-eslint/no-unused-vars
export type Media <T extends MediaType> = {

	/**
	 * The title of the media
	 */
	title: string;

	/**
	 * The URL for more information about the media
	 */
	url: URL;

	/**
	 * The RT score of the media
	 */
	score?: number;

	/**
	 * The year the media was released
	 */
	year?: number;
};
