import type { Media } from '~/models/media';
import type { MediaType } from '~/models/media-type';

/**
 * Describes a TV series
 *
 * @public
 */
export type TVSeries = Media<MediaType.TV_SERIES>;
