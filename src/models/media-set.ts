import type { Media } from '~/models/media';
import type { MediaType } from '~/models/media-type';

/**
 * Describes a collection of Media
 *
 * @public
 */
export type MediaSet <T extends MediaType> = {

	/**
	 * The title of the collection
	 */
	title: string;

	/**
	 * The media in the collection
	 */
	media: Media<T>[];
};
