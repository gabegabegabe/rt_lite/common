import type { Media } from '~/models/media';
import type { MediaType } from '~/models/media-type';

/**
 * Describes a Movie
 *
 * @public
 */
export type Movie = Media<MediaType.MOVIE>;
