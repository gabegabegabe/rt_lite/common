/**
 * Represents possible types of media
 *
 * @public
 */
export enum MediaType {
	MOVIE = 'movie',
	TV_SERIES = 'tv-series'
}
