# @rt_lite/common
Common models and utilities for rt_lite

## Usage
First install the package by running:

```sh
npm i -S @rt_lite/common
```

Then, you may import what you like into your code.  For example:

```ts
import { Media } from '@rt_lite/common/models';
```

## Documentation
Documentation can be found [here](./documentation/index.md).
